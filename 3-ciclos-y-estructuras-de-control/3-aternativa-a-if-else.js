/*
Alternativa a if else.
*/

// Ejemplo:
const hoy = new Date();
let dia = hoy.getDay();

const diasLetras = {
  0: 'domingo',
  1: 'lunes',
  2: 'martes',
  3: 'miercoles',
  4: 'jueves',
  5: 'viernes',
  6: 'sabado',
};
console.log(diasLetras[dia]);

const diasLetras1 = [
  'domingo',
  'lunes',
  'martes',
  'miercoles',
  'jueves',
  'viernes',
  'sabado',
];

console.log(diasLetras1[dia]);
