/*
Lógica booleana.
*/

const regresaTrue = () => {
  console.log('Regresa true');
  return true;
};

const regresaFalse = () => {
  console.log('Regresa false');
  return false;
};

// 1. NOT o la negacion, transformar un valor booleano en su contrario:
console.warn('Not o la negacion.');
const isDev = true;
console.log(!isDev);

console.log(!regresaFalse());
console.log(!regresaTrue());

// 2. AND, &&: regresa true si todos los valores son true, sino false:
console.warn('Operador AND');
const isTrue = true;
const isFalse = false;
console.log(isTrue && isTrue);
console.log(isTrue && isFalse);
console.log(isFalse && isTrue);
console.log(isFalse && isFalse);
console.log(`4 condiciones:`, isTrue && isTrue && isTrue && isFalse);
console.log(isTrue && 'Me muestro porque es true');

// 3. OR, ||: este es el opuesto del AND. Reetorna true si uno de ellos es true.
console.warn('Operador OR');
console.log(isTrue || isTrue);
console.log(isTrue || isFalse);
console.log(isFalse || isTrue);
console.log(isFalse || isFalse);
console.log(`4 condiciones:`, isTrue || isTrue || isTrue || isFalse);

// 4. asignaciones:
console.warn('Asignaciones');
const soyUndefined = undefined;
const soyNull = null;
const soyFalso = false;

const a1 = true && 'Hola Mundo!!!';
console.log(a1);

// Va evaluando y muestra el ultimo que es true:
const a2 = 'Hola' && 'Mundo' && '!!!';
console.log(a2);

const a3 = soyFalso || 'Yo no soy falso';
console.log(a3);

const a4 = soyFalso || soyUndefined || soyNull || 'Ya no soy falso de nuevo';
console.log(a4);
