/*
Operador Ternario: Es un condicional if-else resumido.

Veamos el siguiente ejemplo:
- dias de la semana abrimos a las 11,
- los fines de semana abrimos a las 9.

Vamos a entrar a un sitio web y ver si est abierto:
*/

const date = new Date();

let diaActual = date.getDay();
diaActual = 0;
let hour = date.getHours();
hour = 10;

let horaApertura;
let mensaje;

// if (diaActual === 0 || diaActual === 6) {
// if ([0, 6].includes(diaActual)) {
//   console.log('Fin de semana');
//   horaApertura = 9;
//   console.log('Hora de apertura: ' + horaApertura + ' de la mañana.');
// } else {
//   console.log('Dia de semana');
//   horaApertura = 11;
//   console.log('Hora de apertura: ' + horaApertura + ' de la mañana.');
// }

// Aqui vemos el operador ternario es lo mismo que arriba:
horaApertura = [0, 6].includes(diaActual) ? 9 : 11;

// if (hour >= horaApertura) {
//   mensaje = 'Esta abierto la tienda';
//   console.log(mensaje);
// } else {
//   mensaje = `Esta cerrado, hoy abrimos a las ${horaApertura}`;
//   console.log(mensaje);
// }

// aqui tenemos el operador ternario:
mensaje =
  hour >= horaApertura
    ? 'Esta abierto la tienda.'
    : `Esta cerrado, hoy abrimos a las ${horaApertura}.`;

console.log(mensaje);
