/*
if else.

Veremos la primera estructura de control. Las estructuras de control nos permiten alterar la ejecucion de continuo del programa. La estructura de control nos permite ejecutar código en función de una condicion.
*/

// Ejemplo 1:
let a = 10;
if (a >= 10) {
  console.log('A es mayor de que 10');
} else {
  console.log('A es menor a 10');
}
console.log('FIN DE PROGRAMA');

// Ejemplo 2:
const hoy = new Date();
let dia = hoy.getDay();
console.log(dia);
console.log(typeof dia);
console.log(dia === 2 ? 'Viernes' : 'No es');

if (dia === 0) {
  console.log('Domingo');
} else if (dia === 1) {
  console.log('Lunes');
} else if (dia === 2) {
  console.log('Martes');
} else if (dia === 3) {
  console.log('Miercoles');
} else if (dia === 4) {
  console.log('Jueves');
} else if (dia === 5) {
  console.log('Viernes');
} else if (dia === 6) {
  console.log('Sabado');
}
