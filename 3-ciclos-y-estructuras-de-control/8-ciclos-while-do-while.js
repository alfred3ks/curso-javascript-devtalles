/*
Ciclos.
while y do while.

Los ciclos nos permiten repetir un código varias veces.
Tenemos que tener cuidado con los ciclos infinitos, por eso usamos una variable contador.
*/

// Ciclo while:
console.log('-----while-----');
const car = ['Ford', 'Mazda', 'Honda', 'Toyota'];
let i = 0;
while (i < car.length) {
  console.log(car[i]);
  i++;
}

// Ciclo do while:
console.log('-----Do while-----');
const car2 = ['Chevrolet', 'Hyunday', 'Ferrari', 'Red Bull'];
let j = 0;
do {
  console.log(car2[j]);
  j++;
} while (car2.length > j);
