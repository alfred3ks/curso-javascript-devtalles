/*
Valor por valor y por referencia.

Veremos como pasar información por valor y por referencia.
*/

/*
Cuando trabajamos con primitivos cualquier tipo de asignación como la que hacemos a a=30, o cuando lo mandamos a una funcion como argumento lo estamos mandando por valor. Todos los primitivos son pasados por valor.
*/
let a = 10;
let b = a;
a = 30;

console.log(a, b);

/*
Veamos ahora con un objeto:
En JavaScript todos los objetos son pasados por referencia.
*/
const juan = {
  nombre: 'Juan',
};
const ana = juan;
console.log(juan, ana);
ana.nombre = 'Ana';
console.log(juan, ana);

/*
Veamos ahora con una funcion, vemos como el objeto se envia por referencia. Cualquier modificacion altera el original.
*/

const changeName = (person) => {
  person.name = 'Tony';
  return person;
};

const peter = { name: 'Peter' };
const tony = changeName(peter);
console.log(peter, tony);

/*
Cuando necesitemos que no se modificque el arreglo, el objeto, original debemos hacer una copia del mismo usando el spread operator.
*/
const ana2 = { ...juan };
ana2.nombre = 'Anita';
console.log(juan, ana2);

// Ahora con la funcion, pasamos el objeto con el rest operator.
const peter0 = { name: 'Peter0' };
const changeName0 = ({ ...person }) => {
  person.name = 'Tony el grande';
  return person;
};

const tonyElGrande = changeName0(peter0);
console.log(peter0, tonyElGrande);

/*
Ahora los vemos con los arreglos:
Vemos como pasar una copia para no modificar el original tambien.
Lo mejor usar el spread operator.
*/

const frutas = ['Manzana', 'Pera', 'Pina'];
const otrasFrutas = [...frutas];

// Otra forma de hacerlo:
// const otrasFrutas = frutas.slice();

otrasFrutas.push('Mango');

console.log(frutas, otrasFrutas);
