/*
Ciclos for, ciclo for in, y ciclo for of.
*/

// Ciclo for:
console.log('---for---');
const heroes = ['Batman', 'Superman', 'Aquaman', 'Mujer Maravilla'];
for (let i = 0; i < heroes.length; i++) {
  console.log(heroes[i]);
}

// Ciclo for in: Es lo mismo que el for tradicional:Accede al indice del elemento en el arreglo:
console.log('---for in---');
for (let i in heroes) {
  console.log(heroes[i]);
}

// Ciclo for of: Esto mas simplificado: Accede directamente al valor del arreglo:
console.log('---for of---');
for (let heroe of heroes) {
  console.log(heroe);
}
