/*
Tips de operador ternario.
*/

// Ejemplo 1:
const elMayorNumero = (a, b) => {
  return a > b ? a : b;
};

console.log(elMayorNumero(12, 34));
console.log(elMayorNumero(23, 12));

// Ejemplo 2:
const tieneMenbresia = (isMiembro) => {
  return isMiembro ? '2 dolares' : '10 dolares';
};

console.log(tieneMenbresia(true));
console.log(tieneMenbresia(false));

// Ejemplo 3:
const amigo = true;
const amigosArr = [
  'Rafael',
  'Peter',
  'Tony',
  'Margaret',
  amigo ? 'Lucas' : 'Pedro',
];

console.log(amigosArr);

// Ejemplo 4: Tenemos un if muy simplificado:
const nota = 98;

const grado =
  nota >= 95
    ? 'A+'
    : nota >= 90
    ? 'A'
    : nota >= 85
    ? 'B+'
    : nota >= 80
    ? 'B'
    : nota >= 75
    ? 'C+'
    : nota >= 70
    ? 'C'
    : 'F';

console.log(nota, grado);
