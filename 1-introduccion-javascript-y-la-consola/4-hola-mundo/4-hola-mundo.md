## Tenemos varias formas de ejecutar javascript:

- En el navegador:

Podemos ejecutar tambien código de JavaScript en el navegador en la consola del navegador.
Escribimos este código:

```javascript
console.log('Hola mundo desde JavaScript.');
document.write('Hola Mundo);
```

- Con Node.js
  Para ejecutar el código con node debemos hacerlos con la consola: node app.js:

```javascript
console.log('Hola mundo desde JavaScript.');
```

- Relacionando el archivo index.html por medio de un script.

## index.html:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>My firts code JavaScript</title>
  </head>
  <body>
    <script src="./app.js"></script>
  </body>
</html>
```

## app.js:

```javascript
console.log('Hola mundo desde JavaScript.');
```
