/*
Problemas de iniciar variables con var:

Si declaramos variables con var se puede reemplazar propiedades y objetos propios del objeto global window.

Vemos como podemos acceder al ancho y alto de la pantalla,

window.outerWidth;
window.outerHeight;

Se ejecutamos esas varaibles vemos como nos devuelve el ancho y el alto de la pantalla. Ahora si nosotros definimos una variable con var igual que esas lo que hacemos es machacar la del objeto window y ahora tendremos un problema.

Si ejecutamos en el navegador ahora:
window.outerWidth;

Veremos como cambia nuestra pantalla al ancho que tenemo especificado en la variable. Esto nos puede dar muchos problemas.

Con let y const esto no pasa, no sobre escriben el objeto global window.

Tambien var tiene la peculiaridad de hacer hoisting, eso a veces nos puede hacer comportamientos raros y no podemos hacer control de las variables, recomendación declarar variables con let y const.

*/

console.log('Hola Mundo!!!');

// Podemos definir una variable y esta se coloca en el scope global y podemos acceder a ella desde otro archivo
var miNombre = 'Fernando';

// var outerWidth = 1200;
let outerWidth = 1200;
