/*
Orden y lugar de las importaciones:
El orden de como importemos un archivo .js importa.

No es igual colocar nuestro script de archivos javascript en la cabecera que al final del body. La carga de archivos la hara el navegador leyendo linea por linea y ira ejecutando. Si colocamos el script en la cabecera como es el caso y ponemos una funcion bloqueante como es un alert() el resto del código no se ejecutara hasta que el usuario actue sobre el alert. En cambio si esta al final el primero cargara el html y luego el script.

Lo podemos ver en la consola de desarrollar. Como carga o no el html.

NOTA: Para organizar mejor muestro código lo ideal es crear una carpeta llamada assets y dentro poner otra carpeta js, donde meteremos nuestros archivos de javascript, tambien lo podemos hacer con el css.

Podemos crear una variable en archivo app.js y podemos acceder al valor de dicha variable ya que esta se registra en el scope global, si la buscamos en windows veremos que ahi esta definida.

Esto no es buena práctica. Con los módulos eso se soluciona.

Ahora veamos si invertimos los script en el html, primero llamamos el de alert que el de app, vemos como nos da error, y nos dice que la varaibles miNombre no esta definida. Por eso el orden importa.

*/

alert('eyyyyy!!!');

console.log('Hola Mundo!!!');

// Podemos definir una variable y esta se coloca en el scope global y podemos acceder a ella desde otro archivo
var miNombre = 'Fernando';
