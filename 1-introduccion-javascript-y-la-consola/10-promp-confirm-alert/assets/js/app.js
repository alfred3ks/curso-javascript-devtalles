/*
Promp, confirm, alert.
Tenemos varias formas de acceder información por parte del usuario, no son muy usadas, pero debemos saber que existen.

alert():
Funcion bloqueante que manda un mensaje.

prompt():
Funcion que saca una caja donde se puede meter información.

confirm():
Función que pide confirmación al usuario

Todos estas funciones pertenecen al objeto windows. Estas funciones funcionan en el navegador, pero en Node.js no. En node existe el objeto global.

*/

alert('Hola mundo');

let nombre = prompt('¿Cual es tu nombre?');
console.log(nombre); // null si cancela

const confirmacion = confirm('Estas seguro de esto');
console.log(confirmacion); // true o false
