/*
Introducción a la consola:

JavaScript es un lenguaje interpretado, donde corre JavaScript se ejecuta linea por linea una detras de la otra hasta final del archivo de manera secuencial, los console.log() son usados para no interferir en la ejecución del programa.

El poder imprimir mensajes en consola nos ayuda a ver el valor de una variable en un determinado punto.

Dentro del console.log podemos pasar varios paramatros.

*/

let a = 10;
let b = 10;
let c = 10;
let d = 'Hola ';
let e = 'Spiderman';

const saludo = d + e;

console.log('a:', a);
console.log('b:', b);
console.log('c:', c);

// Esta es ptra forma de hacerlo y no hace falta pasar sino solo la variable entre llaves:{}, es como si crearamos un objeto

// Tambien podemos imprimir con estilos en la consola:
console.log('%c Mis variables', 'color:crimson; font-weight:bold');
console.log({ a });
console.log({ b });
console.log({ c });

// Podemos usar el console.table() para imprimir en forma de tabla:
console.table({ d, e });

console.log(saludo);
