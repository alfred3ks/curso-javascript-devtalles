# Historia de JavaScript.

A inicio de los años 90 la velocidad de los moden era de 28kbps, esta velocidad nos ayudo a crear app web mas complejas. Con el nacimiento de web mas potentes y un internet tan lento surgio la necesidad de un lenguaje de programación que se ejecutara del lado del cliente.

La idea era validar la informacion en el cliente antes de ser enviada al servidor, asi nos evitariamos mas peticiones al servidor y con un internet tan lento la experiencia seria bastante mala.

Para eso nacio JavaScript, fur creado por Brendan Eich, en sus inicios el lenguaje se llamaba LiveScript. Con fines de marketing se le llamo JavaScript.

Para el año 1997 Nextcape decidio extandarizar el lenguaje. Se envia la especificación del lenguaje javascript 1.1 a la asociacion europea de fabricantes de computadoras, la cual conocemos como ECMA, European Computer Manufacturers Association.

ECMA crea un comite para standarizar el lenguaje. El encargado de estandarizar el lenguaje fue el comite ECMA TC39. Este comite creo el primer estandart del lenguaje llamado ECMA-262 el cual lo definio como el lenguaje ECMAScript.
