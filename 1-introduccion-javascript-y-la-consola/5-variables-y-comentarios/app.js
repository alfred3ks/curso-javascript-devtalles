/*
Variables y comentarios:

Los comentarios son lineas de código que el intérprete de JavaScript ignorará a la hora de ejecutarlo.

Para comentarios de una linea con la doble barra //

Para comentarios de varias lineas como lo vemos en esta explicación.
*/

/*
Las variables:
Una variable no es mas que un contenedor de información que apunta a un lugar en memoria. dicha información puede cambiar en un futuro.

Usamos la palabra let para declarar variables. Tambien podemos ver declaración de variables con var, a dia de hoy debemos evitarlo. Pero podemos ver tema legacy que lo implementa.

Las constantes:
Una constante es tambien un contenedor de informacion que apunta a un lugar en memoria, a diferencia de las variables su valor no puede cambiar en el futuro. Si tratamos de hacerlo recibiremos un error.

Usamos la palabra const para declarar constantes.

NOTA: Podemos acceder a estas variables/constantes en la consola del navegador, pero si nos script tiene el type='module' no podremos.
*/

let a = 10;
let b = 10;
const c = 10;
let x = a + b;

console.log(x);

// Podemos definir vaariables separadas por comas usando una sola instrucción let:
let z = 22,
  w = 23,
  m = 16,
  v = 16,
  d = z + w + m + x;

console.log(d);
