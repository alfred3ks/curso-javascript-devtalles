# Usos de JavaScript.

JavaScript nacio para poder mandar la información al servidor lo más procesada posible.
Hoy en dia JavaScript es un monopolio, sin importar que corre en el servidor JavaScript lo controla todo en un 99%.

Hace muchos años JavaScript solo corria en el cliente, a dia de hoy ya no es asi, ya podemos usarlo en el servidor, gracias a Nodejs, asi ya corre de manera independiente.

Podemos ver javascript en proyectos:

- Aplicaciones web,
- Crear presentaciones:
  https://revealjs.com/
- Web server, o lo que es lo mismo servidores web, con Node.js
- Video juegos:
  https://js13kgames.com/games/underrun/index.html
- Aplicaciones moviles usando React native, ionic, NativeScript.
