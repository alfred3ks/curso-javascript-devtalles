/*
Depuracion y breakpoints:
Si alguna ves tenemos problemas con el código debemos colocar un brekpoint, que es lo mismo que un punto de parada de nuestro código para ver que esta haciendo en ese punto y poder depurar errores.

Para eso debemos ir al navegador, a la pestaña se source(fuentes) y poner puntos de parada en nuestro archivo que deseemos revisar.

La consola del navegador nos provee de esta herramienta muy importante en el desarrollo.

Tambien podriamos depurar y poner breakpoints en nuestro archivo .js y ejecutarlo con vscode. En el menu superior debemos ejencutar el depurador.
*/

let a = 10;
let b = 10;
let c = 10;
let d = 'Hola ';
let e = 'Spiderman';

const saludo = d + e;

console.log('a:', a);
console.log('b:', b);
console.log('c:', c);

// Esta es ptra forma de hacerlo y no hace falta pasar sino solo la variable entre llaves:{}, es como si crearamos un objeto

// Tambien podemos imprimir con estilos en la consola:
console.log('%c Mis variables', 'color:crimson; font-weight:bold');
console.log({ a });
console.log({ b });
console.log({ c });

// Podemos usar el console.table() para imprimir en forma de tabla:
console.table({ d, e });

console.log(saludo);
