/*
Funciones.
En JavaScript, una función es un bloque de código que realiza una tarea específica o calcula un valor. Las funciones son un aspecto fundamental del lenguaje y se utilizan para organizar y reutilizar el código. Pueden aceptar parámetros como entrada y devolver un resultado como salida.

En JavaScript, existen varios tipos de funciones que se utilizan de diversas maneras para realizar tareas específicas. Estos son algunos de los tipos más comunes de funciones:

1. Funciones declarativas (o por declaración):
function sumar(a, b) {
  return a + b;
}

- **Características:**
  - Se declaran utilizando la palabra clave `function`.
  - Tienen un nombre asociado.
  - Se pueden llamar antes de su declaración (concepto conocido como hoisting), ya que JavaScript mueve las declaraciones de funciones al inicio del contexto de ejecución.

2. Expresiones de función (funciones anónimas):
let resta = function(a, b) {
  return a - b;
};

- **Características:**
  - Se asignan a una variable, por lo tanto, son anónimas.
  - Pueden ser utilizadas como cualquier otro valor en JavaScript.
  - No pueden ser llamadas antes de su definición debido a que no tienen nombre (no son hoisted).

3. Funciones flecha (arrow functions):
let multiplicar = (a, b) => a * b;

- **Características:**
  - Son una forma más concisa de escribir funciones en JavaScript.
  - No tienen su propio `this`, `arguments`, `super` o `new.target`. Esto significa que heredan estos valores del contexto en el que están definidas.
  - Son especialmente útiles en funciones de devolución de llamada (callback functions) y funciones de orden superior (higher-order functions).

4. Funciones de devolución de llamada (callback functions):
Son funciones que se pasan como argumentos a otras funciones para ser ejecutadas después de que ocurra algún evento o se complete una tarea.

Ejemplo:
function mostrarMensaje(callback) {
  console.log('Hola, soy la función mostrarMensaje.');
  callback();
}

function mensajeFinal() {
  console.log('Mensaje final.');
}

mostrarMensaje(mensajeFinal); // Se pasa mensajeFinal como una función de devolución de llamada

5. Funciones de orden superior (higher-order functions):
Son aquellas funciones que toman una o más funciones como argumentos y/o devuelven otra función.

Ejemplo:
function operacionMatematica(func, a, b) {
  return func(a, b);
}

function multiplicar(a, b) {
  return a * b;
}

let resultado = operacionMatematica(multiplicar, 5, 3); // Se pasa la función multiplicar como argumento
console.log(resultado); // Salida: 15

Estos son algunos de los tipos fundamentales de funciones en JavaScript, cada uno con sus propias características y usos particulares. La elección de qué tipo de función utilizar depende del contexto y los requisitos específicos de tu aplicación.

En el contexto de las funciones en programación, es importante entender la diferencia entre los términos "parámetros" y "argumentos":

Parámetros:
Los parámetros son nombres utilizados dentro de la declaración de una función. Son variables locales que representan los valores que una función espera recibir cuando es invocada. Se definen entre paréntesis en la declaración de la función.

Argumentos:
Los argumentos son los valores reales que se pasan a una función cuando se invoca. Son los valores que se asignan a los parámetros de la función cuando se llama a esa función.

Las funciones de declaracion no las de expresion tienen implicito dentro los argumentos que reciben por medio del objeto arguments.

*/

// Funcion de declaracion:
function saludar() {
  console.log('Hola mundo!!!');
}
saludar();

// Funcion anonima, de expresion:
const saludo = function () {
  console.log('Hola mundo!!!');
};
saludo();

// Paso de argumentos y parametros a una funcion:
// la función recibe los parametros:
// OJO arguments solo en funciones de declaracion:
function saludarHumano(nombre) {
  console.log(arguments);
  console.log('hola ' + nombre);
}

saludarHumano('Mario', 40, true, 'Costa Rica'); // Aqui pasamos los argumentos

// Funcion anonima arrow function de expresion:
const says = (nombre) => {
  console.log(`Hola ${nombre}`);
};
says('Luis');
