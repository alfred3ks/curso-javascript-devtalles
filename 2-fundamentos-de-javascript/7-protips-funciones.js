/*
Tips:
- Funciones,
- Argumentos,
- Desestructuración de objetos.
Vamos a ver como pulir la escritura en javascript.
*/

/*
1 - Podemos hacer un return de un objeto lo mas simplificado posible:
*/
const createPerson = (name, lastName) => ({ name, lastName });
const person = createPerson('Fernando', 'Valenzuela');
console.log(person);

/*
2- Arguments en funciones flecha.
La diferencia radica en la disponibilidad del arreglo arguments. Las funciones normales tienen acceso directo a arguments, mientras que las funciones de flecha no lo tienen, ya que utilizan la vinculación léxica y no tienen su propio contexto de arguments.
Si queremos usar los argumentos en una funcion flecha lo debemos pasar nosotros por parametro. Lo haremos con el operador spread operator. si usamos el operador spread luego a la funcion no podemos pasar mas argumentos. si queremos lo podemos hacer pasar antes entonces tomaria el primer valor pasado y el resto lo meteria en ...arg.
*/
function printerArguments() {
  console.log(arguments);
}
printerArguments(10, true, false, 'Maria');

const printerArguments2 = (edad, ...arg) => {
  console.log(edad);
  console.log(arg);
};
printerArguments2(22, 345, 'Luis', true, ['hola', 'Mundo']);

/*
3 - Vamos a desestructurar el arreglo que retorna arguments:
*/
const desestructArg = (...arg) => {
  return arg; // Retorna un []
};
const [nombre, vivo, casado, edad] = desestructArg('Luis', true, false, 34);
console.log(nombre);
console.log(vivo);
console.log(casado);
console.log(edad);

/*
Desestructuración de argumentos, podemos por argumentos pasar aquellos valores que deseemos del objeto, y tambien podemos pasar valores por defecto.
*/

let tony = {
  nombre: 'Tony Stark',
  codeName: 'Iroman',
  vivo: false,
  edad: 40,
  trajes: ['Mark I', 'Mark V', 'Hulkbuster'],
};

const printProps = ({ nombre, codeName, vivo, edad = 15, trajes }) => {
  console.log(nombre);
  console.log(codeName);
  console.log(vivo);
  console.log(edad);
  console.log(trajes);
};

printProps(tony);
