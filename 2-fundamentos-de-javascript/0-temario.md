# Temario de esta sección:

- ¿Qué son los primitivos?
- Palabras reservadas
- Arreglos
- Objetos literales
- Funciones básicas
- Funciones de flecha
- Retorno de las funciones
- Ejercicios y ejemplos con cada tipo expuesto
