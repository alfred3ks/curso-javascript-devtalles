/*
Retorno de funciones.
Todas las funciones cuando la creamos retornan un valor. Si no colocamos el retorno la funcion retornara undefined, osea no retorna nada.

Asi como tenemos declarada la función si quitamos el retorn vemos como la variable respuesta que guarda lo que retorna la funcion da undefined. Si colocamos el retorn eso se llama implicito, osea que tiene un retorn implicito.

Todo código que pongamos siguiente a la intrucción return no se ejecutara.

Dentro de una función podemos retornar cualquier cosa, un booleano, un número, un string, un objeto, u otra función.
*/

const saludar = (nombre) => {
  console.log(nombre);
  // return `Hola ${nombre}`;
};

// Guardamos la respuesta que retorna la función:
const respuesta = saludar('Luis');
console.log(respuesta);

// Varios valores de return:
const suma1 = (n1, n2) => {
  return n1 + n2;
};

const suma2 = (n1, n2) => {
  return {
    n1,
    n2,
  };
};

const suma3 = (n1, n2) => {
  return [n1 * 2, n2 / 5];
};

const suma4 = (n1, n2) => {
  let result = true;
  if (n1 > n2) {
    result = false;
  }
  return result;
};

let respuestaTest = suma1(1, 2);
console.log(respuestaTest);

respuestaTest = suma2(1, 2);
console.log(respuestaTest);

respuestaTest = suma3(1, 2);
console.log(respuestaTest);

respuestaTest = suma4(1, 2);
console.log(respuestaTest);

const aleatorio = () => Math.round(Math.random() * 100);

console.log(aleatorio());
