/*
Arreglos.
Los arreglos son un objeto muy parecido a una lista de información, que contienen un grupo de elementos.

Usualmente, esa información dentro del arreglo es del mismo tipo de dato. Pero en JavaScript es posible tener diferentes tipos de datos dentro de un arreglo.

En JavaScript, un arreglo (array) es una estructura de datos que se utiliza para almacenar una colección ordenada de elementos. Estos elementos pueden ser de cualquier tipo, como números, cadenas de texto, objetos, funciones u otros arreglos. Los arreglos en JavaScript son objetos especiales diseñados para almacenar datos secuencialmente bajo un solo identificador.

Algunos conceptos clave sobre los arreglos en JavaScript son:

Declaración de un arreglo:
Puedes declarar un arreglo de varias formas:

1. Notación de corchetes ([]),
2. Utilizando el constructor Array(). Esta es poco común,

Los elementos dentro de un arreglo se acceden mediante su índice, que comienza desde cero para el primer elemento y aumenta en uno para cada elemento subsiguiente.

*/

// Definimos un arreglo usando el construcctor: Internamente tiene 10 espacios vacios:
const arr = new Array(10);
console.log(arr);

// Definimos un arreglo usando la notación de corchetes:
const videoJuegos = ['Mario3', 'Megaman', 'Chrono Trigger'];
console.log({ videoJuegos }); // Arreglo de 3 elementos.
console.log(videoJuegos[0]);
console.log(videoJuegos[1]);
console.log(videoJuegos[2]);

console.log(videoJuegos.length);

// Creamos un array de diversos tipos de datos:
const arrCosas = [
  true,
  123,
  'Fernando',
  1 + 2,
  function () {},
  () => {},
  { a: 1 },
  ['X', 'Megaman', 'Zero', 'Dr. Light', ['Dr. Willy', 'Woodman']],
];

console.log({ arrCosas });
console.log(arrCosas[0]);
console.log(arrCosas[1]);
console.log(arrCosas[2]);
console.log(arrCosas[3]);
console.log(arrCosas[7][3]);
console.log(arrCosas[7][4][0]);
console.log(arrCosas[7][4][1]);

console.log(arrCosas.length);

// Propiedades y métodos básicos de los arreglos:
let juegos = ['Zelda', 'Mario', 'Metroid', 'Chrono'];

// Propiedad .length:
console.log('largo:', juegos.length); // Vemos la longitud del arreglo.
const primero = juegos[0];
console.log(primero);
const ultimo = juegos[juegos.length - 1];
console.log(ultimo);

// Para iterar el arreglo: Mostramos todos los elementos del arr:
juegos.forEach((juego, indice, arr) => {
  console.log({ Juego: juego, 'Indice:': indice, Arreglo: arr });
});

// Método para insertar elementos al final de arreglo:
juegos.push('F-Zero');
console.log(juegos);
console.log(juegos.length);

// Metodo para insertar un elemento al inicio del arreglo:
juegos.unshift('Fire Emblem');
console.log(juegos);
console.log(juegos.length);

// Para borrar elemento, por ejemplo el ultimo elemento:
const juegoBorrado = juegos.pop();
console.log(juegoBorrado, juegos);
console.log(juegos.length);

// Para borrar elemento, por ejemplo el primer elemento:
juegos.shift();
console.log(juegos);
console.log(juegos.length);

// Para borrar el segundo elemento:
const position = 1;
console.log(juegos);
const juegosBorrados = juegos.splice(position, 3);
console.log(juegos);
console.log(juegosBorrados);

// Como saber el indice de un elemento de un array:
juegos = ['Zelda', 'Mario', 'Metroid', 'Chrono'];
const indexMetroid = juegos.indexOf('Metroid');
console.log(indexMetroid); // Si el elemento no existe retorn -1.
