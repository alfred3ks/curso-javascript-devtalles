/*
Tipos de datos primitivos.

El tipo dedato de una variable:
JavaScript es un lenguaje debilmente tipado, explicitmente no se le dice a JavaScript el tipo de una varaible, Javascript lo va a inferir por nosotros.

El tipo de dato de una variable describe el contenido del valor que tiene la variable.

Los primitivos es una información que no es un objeto y son inmutables. Existen 6 tipos de datos primitivos en JavaScript:

- Boolean, true/false,
- null, sin valor en lo absoluto,
- Undefined, Una variable declarada que aun no se le asigna valor,
- Number, integer, floats, etc,
- String, son cadenas de carcteres,
- Symbol, es un tipo de dato unico que no es igual a ningun valor.
*/

// String:
let nombre = 'Peter Parker';
console.log(nombre);
nombre = 'Ben Parker';
console.log(nombre);
nombre = 'Tia May';
console.log(nombre);
nombre = `Tia May`;
console.log(nombre);

console.log(typeof nombre); // string

// Boolean:
let esMarvel = true;
console.log(esMarvel);
esMarvel = false;
console.log(esMarvel);
console.log(typeof esMarvel);

// Number:
let edad = 24;
console.log(edad);
edad = 25;
console.log(edad);
console.log(typeof edad); // Number
edad = 25.003;
console.log(typeof edad); // Number

// undefined:
let superPoder;
console.log(typeof superPoder);

// null:
let soyNull = null;
console.log(typeof soyNull); // object.

// Symbol:
let symbol1 = Symbol('a');
let symbol2 = Symbol('a');

console.log(typeof symbol1);
console.log(typeof symbol2);

console.log(symbol1 === symbol2); // false
