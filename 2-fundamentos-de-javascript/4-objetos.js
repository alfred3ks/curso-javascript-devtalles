/*
Objetos literales. Los objetos {}
En JavaScript, los objetos literales son una forma de crear objetos de manera sencilla y directa. Se definen utilizando una sintaxis que consiste en pares de clave-valor, donde una clave es una cadena (también conocida como propiedad) y el valor puede ser cualquier tipo de dato válido en JavaScript (números, cadenas de texto, arreglos, funciones u otros objetos).
*/

let personaje = {
  nombre: 'Tony Stark',
  codeName: 'Iroman',
  vivo: false,
  edad: 40,
  coords: {
    lat: 34.034,
    lng: -118.7,
  },
  trajes: ['Mark I', 'Mark V', 'Hulkbuster'],
  direccion: {
    zip: '10880, 90265',
    ubicacion: 'Malibu, California',
  },
  ultimaPelicula: 'Infinity War',
};

console.log(personaje);

// Asi accedemos a los valores del arreglo: Usando la nomenclatura del punto.
console.log('Nombre:', personaje.nombre);

// Tambien podemos acceder por medio de la notación corchete:
console.log('Nombre:', personaje['nombre']);
console.log('Edad:', personaje['edad']);

// Vamos a obtener los datos anidados:
console.log('Coordenadas:', personaje.coords);
console.log('Latitud:', personaje.coords.lat);
console.log('Longitud:', personaje.coords.lng);

console.log('N.trajes:', personaje.trajes.length);
console.log('Ultimo traje:', personaje.trajes[personaje.trajes.length - 1]);

// Esto es lo mismo que acceder por medio de la notación de corchete pero pasandole una variable:
const x = 'vivo';
console.log('Vivo:', personaje[x]);

console.log('Ultima película:', personaje.ultimaPelicula);

// Mas detalles de los objetos:
personaje = {
  nombre: 'Tony Stark',
  codeName: 'Iroman',
  vivo: false,
  edad: 40,
  coords: {
    lat: 34.034,
    lng: -118.7,
  },
  trajes: ['Mark I', 'Mark V', 'Hulkbuster'],
  direccion: {
    zip: '10880, 90265',
    ubicacion: 'Malibu, California',
  },
  ultimaPelicula: 'Infinity War',
};

// Para borrar una propiedad del objeto: Usando la palabra delete:
delete personaje.edad;
console.log(personaje);

// Transformar el objeto a arreglo: Crea un arreglo con mas arreglos dentro:
const entriesPares = Object.entries(personaje);
console.log(entriesPares);

// Para crear una nuevo propiedad en el objeto: esto se genera en tiempo de ejecución:
personaje.casado = true;
console.log(personaje);

// Asi podemos proteger al objeto, lo congelamos: No podemos modificar ni agregar nuevas propiedades: OJO pero si podemos modificar objetos internos:
Object.freeze(personaje);

// Si tratamos de modificarlo no lo hace porque esta congelado:
personaje.dinero = 100000000000;
personaje.direccion.ubicacion = 'Costa Rica';
console.log(personaje);

// Listaremos todas las propiedades/valores del objeto: retorna un []
const propiedades = Object.getOwnPropertyNames(personaje);
const valores = Object.values(personaje);
console.log(propiedades);
console.log(valores);
