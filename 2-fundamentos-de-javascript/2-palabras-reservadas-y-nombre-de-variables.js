/*
Palabras reservadas y nombre de variables.

NOTA: Para nombrar archivos la recomendación hacerlo usando guion medio, como lo estamos haciendo.

Palabras reservadas:
Son palabras que tienen un uso especifico dentro del lenguaje de programación. Tenemos un documento llamado palabrasreservadas.pdf donde podemos ver esas palabras del lenguaje de programación.

Para la creación de variables debemos seguir estas reglas:

- No podemos declarar variables que empiecen con numeros.
En JavaScript, hay varias formas de declarar variables, y las reglas para hacerlo son las siguientes:

1. **Palabras clave para declarar variables:** En JavaScript, se utilizan tres palabras clave para declarar variables: `var`, `let`, y `const`.
- `var`: Antes de la aparición de `let` y `const`, `var` era la forma principal de declarar variables. Sin embargo, tiene ciertas peculiaridades en su alcance (scope) y puede llevar a problemas como el hoisting.

- `let`: Introducido en ECMAScript 6 (también conocido como ES6), `let` permite declarar variables con un alcance limitado al bloque donde se definen. Es preferible a `var` en muchas situaciones, ya que evita problemas de hoisting y tiene un alcance más claro.

- `const`: También introducido en ECMAScript 6, `const` se usa para declarar variables cuyo valor no cambiará una vez asignado. Es una forma de declarar constantes. El valor de una variable constante no puede ser reasignado.

2. **Reglas de nomenclatura:**
- Los nombres de las variables pueden contener letras, dígitos, guiones bajos (`_`) o signos de dólar (`$`).
- El primer carácter no puede ser un dígito.
- JavaScript es "case-sensitive", lo que significa que distingue entre mayúsculas y minúsculas. Por ejemplo, `miVariable` y `mivariable` se consideran nombres de variables diferentes.
- Es buena práctica utilizar nombres descriptivos para las variables (por ejemplo, `nombre`, `edad`, `resultado`).
- Lo normal es camelcase para declarar variables de mas de una palabra, ejemplo: isDeveloper, nuevaContrasena, etc.
*/

let isDeveloper = true;
console.log(isDeveloper);
